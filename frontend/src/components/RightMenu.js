import React, { Component } from "react";
import { Menu } from "antd";
import { Link } from "react-router-dom";

import { AppstoreOutlined } from "@ant-design/icons";

class RightMenu extends Component {
  render() {
    return (
      <Menu
        mode="horizontal"
        style={{ backgroundColor: "#FEF200", color: "white" }}
      >
        <Menu.Item key="mail">
          <Link to="/" className="menu-top">
            HOME
          </Link>
        </Menu.Item>
        <Menu.Item key="protools">
          <Link to="/project" className="menu-top">
            PARENTING REWARDS
          </Link>
        </Menu.Item>
        <Menu.Item key="blog">
          <Link to="/about" className="menu-top">
            REWARDS
          </Link>
        </Menu.Item>
        <Menu.Item key="blog">
          <Link to="/about" className="menu-top">
            PROMO
          </Link>
        </Menu.Item>
        <Menu.Item key="blog">
          <Link to="/about" className="menu-top">
            CONTACT US
          </Link>
        </Menu.Item>
        <Menu.Item key="blog">
          <Link to="/about" className="menu-top">
            BUY NOW
          </Link>
        </Menu.Item>
        <Menu.Item key="help">
          <a
            href="https://www.linkedin.com/company/properingroup/about/"
            target="_blank"
            className="menu-top"
          >
            <AppstoreOutlined style={{ fontSize: "1.4vw", marginRight: 25 }} />
          </a>
        </Menu.Item>
        <Menu.Item key="blog">
          <Link to="/about" className="menu-top">
            MASUK
          </Link>
        </Menu.Item>
        <Menu.Item key="blog">
          <Link to="/about" className="menu-top">
            DAFTAR
          </Link>
        </Menu.Item>
      </Menu>
    );
  }
}

export default RightMenu;
