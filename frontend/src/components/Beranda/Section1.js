import React, { Component } from "react";
import { Row, Col, Typography, Input } from "antd";
import AOS from "aos";
import { inject, observer } from "mobx-react";
import Slider from "react-slick";

const { Search } = Input;

@inject("ArticlesStore")
@observer
class Section1 extends Component {
  state = {
    width: window.innerWidth,
    visible: false,
    confirmLoading: false,
  };
  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50,
    });
  };
  handleOk = () => {
    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  render() {
    const settings = {
      autoplay: true,
      pauseOnHover: false,
      dots: true,
      dotsClass: "slick-dots",
    };
    return (
      <div>
        <Row className="section1" type="flex" justify="center" align="middle">
          <Col xs={24} md={24}>
            <Slider {...settings}>
              <div>
                <img
                  style={{ width: "100%" }}
                  src="https://parentingrewards.dancow.co.id/snrcms/public/images_new/banners/Dancow-Webbanner-Nutri-Eksplorasi.jpg"
                />
              </div>
              <div>
                <img
                  style={{ width: "100%" }}
                  src="https://parentingrewards.dancow.co.id/snrcms/public/images_new/banners/Dancow-Webbanner-Nutri-Optimalkan.jpg"
                />
              </div>
              <div>
                <img
                  style={{ width: "100%" }}
                  src="https://parentingrewards.dancow.co.id/snrcms/public/images_new/banners/Dancow-Webbanner-Nutri-Lindungi.jpg"
                />
              </div>
              <div>
                <img
                  style={{ width: "100%" }}
                  src="https://parentingrewards.dancow.co.id/snrcms/public/images_new/banners/DancowWebbannerWhatsapp5.png"
                />
              </div>
            </Slider>
          </Col>
        </Row>
        <Row align="middle" className="feature-properin">
          <Col md={12} align="center">
            <Typography
              style={{
                fontSize: "1.2vw",
                fontWeight: "bold",
                color: "#4A4A4A",
              }}
            >
              Temukan kode unik pada kemasan DANCOW
            </Typography>
            <Search
              placeholder="Kode unik [contoh: QEX1234567]"
              enterButton="Submit"
              size="large"
            />
          </Col>
          <Col md={6} align="center">
            <Typography>a</Typography>
            <Typography style={{ fontSize: "1.2vw" }}>
              Pilih Hadiah Sekarang
            </Typography>
          </Col>
          <Col md={6} align="center">
            <Typography>a</Typography>
            <Typography style={{ fontSize: "1.2vw" }}>
              Referensikan Teman
            </Typography>
          </Col>
        </Row>
        <Row className="row-sect1" justify="center">
          <Col md={6} align="center" justify="center">
            <Typography
              style={{
                fontSize: "1.5vw",
                color: "#FF0000",
                fontWeight: "bold",
              }}
            >
              Langkah Mudah
            </Typography>
            <Typography style={{ fontSize: "1.2vw", color: "#D4BF30" }}>
              Untuk Dapatkan Hadiah
            </Typography>
          </Col>
          <Col md={6} align="center" justify="center">
            <Typography style={{ fontSize: "1.2vw", color: "#D4BF30" }}>
              Daftar
            </Typography>
          </Col>
          <Col md={6} align="center" justify="center">
            <Typography style={{ fontSize: "1.2vw", color: "#D4BF30" }}>
              Submit Kode
            </Typography>
          </Col>
          <Col md={6} align="center" justify="center">
            <Typography style={{ fontSize: "1.2vw", color: "#D4BF30" }}>
              Tukar
            </Typography>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Section1;
