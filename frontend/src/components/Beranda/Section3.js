import React, { Component } from "react";
import { Row, Col, Typography, Card, Button } from "antd";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import AOS from "aos";

const { Title } = Typography;

const ornament = require("../../assets/ornament.png");

@inject("ArticlesStore")
@observer
class Section3 extends Component {
  state = {
    visible: false,
    confirmLoading: false,
  };
  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50,
    });
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = () => {
    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  render() {
    return (
      <div>
        <Row className="section3" justify="center">
          <Col
            data-aos="slide-right"
            data-aos-delay="550"
            xs={24}
            align="center"
            style={{ marginBottom: "2.5rem" }}
          >
            <Title level={4} style={{ fontSize: "1.9vw", color: "#D4BF30" }}>
              Artikel Untuk Bunda
            </Title>
            <img src={`${window.location.origin}/images/ornament.png`} />
            <img src={`${window.location.origin}/images/ornament.png`} />
            <img src={`${window.location.origin}/images/ornament.png`} />
          </Col>
          <Row
            type="flex"
            gutter={48}
            justify="space-around"
            data-aos="slide-right"
            style={{ marginBottom: "2.5rem" }}
          >
            <Col md={6} align="center">
              <Link to={`/news/1`}>
                <Card className="news-place">
                  <div className="news-article">
                    <Typography className="header-news">a</Typography>
                  </div>
                </Card>
              </Link>
            </Col>
            <Col md={6} align="center">
              <Link to={`/news/1`}>
                <Card className="news-place">
                  <div className="news-article">
                    <Typography className="header-news">a</Typography>
                  </div>
                </Card>
              </Link>
            </Col>
            <Col md={6} align="center">
              <Link to={`/news/1`}>
                <Card className="news-place">
                  <div className="news-article">
                    <Typography className="header-news">a</Typography>
                  </div>
                </Card>
              </Link>
            </Col>
          </Row>
          <Col
            data-aos="slide-right"
            data-aos-delay="550"
            md={24}
            align="center"
          >
            <Button type="primary" shape="round" size="large">
              Lihat Semua
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Section3;
