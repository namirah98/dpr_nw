import React, { Component } from "react";
import { Row, Col, Typography, Button } from "antd";
import AOS from "aos";

const { Title } = Typography;
const ornament = require("../../assets/ornament.png");

class Section2 extends Component {
  state = {
    width: window.innerWidth,
    visible: false,
    confirmLoading: false,
  };
  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50,
    });
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = () => {
    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  render() {
    return (
      <div>
        <Row className="section2" justify="center">
          <Col
            data-aos="slide-right"
            data-aos-delay="550"
            md={24}
            align="center"
            style={{ marginBottom: "3rem" }}
          >
            <Typography
              level={4}
              style={{ fontSize: "1.9vw", color: "#D4BF30" }}
            >
              Tukarkan Berbagai
            </Typography>
            <Typography
              level={4}
              style={{
                fontSize: "1.9vw",
                color: "#FF0000",
                fontWeight: "bold",
              }}
            >
              Hadiah Menarik
            </Typography>
          </Col>
          <Row
            type="flex"
            gutter={48}
            justify="space-around"
            data-aos="slide-right"
            style={{ marginBottom: "3rem" }}
          >
            <Col md={6} align="center">
              <img
                src={`${window.location.pathname}images/invest1.png`}
                className="img1"
              />
              <Title level={4} className="text-white">
                Cosmos Rice Cooker
              </Title>
              <Typography style={{ fontSize: "1.2vw" }}>28.000 Poin</Typography>
            </Col>
            <Col md={6} align="center">
              <img
                src={`${window.location.pathname}images/invest2.svg`}
                className="img2"
              />
              <Title level={4} className="text-white">
                Dana
              </Title>
              <Typography style={{ fontSize: "1.2vw" }}>5.000 Poin</Typography>
            </Col>
            <Col md={6} align="center">
              <img
                src={`${window.location.pathname}images/invest3.svg`}
                className="img3"
              />
              <Title level={4} className="text-white">
                Vivo
              </Title>
              <Typography style={{ fontSize: "1.2vw" }}>
                120.000 Poin
              </Typography>
            </Col>
            <Col md={6} align="center">
              <img
                src={`${window.location.pathname}images/invest4.svg`}
                className="img4"
              />
              <Title level={4} className="text-white">
                Sharp Oven
              </Title>
              <Typography style={{ fontSize: "1.2vw" }}>90.000 Poin</Typography>
            </Col>
          </Row>
          <Col
            data-aos="slide-right"
            data-aos-delay="550"
            md={24}
            align="center"
          >
            <Button type="primary" shape="round" size="large">
              Lihat Semua
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Section2;
