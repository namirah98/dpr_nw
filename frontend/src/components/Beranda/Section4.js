import React, { Component } from "react";
import { Row, Col, Typography, Card } from "antd";
import { inject, observer } from "mobx-react";
import { Link } from "react-router-dom";
import AOS from "aos";

const { Title } = Typography;

const ornament = require("../../assets/ornament.png");

@inject("ArticlesStore")
@observer
class Section4 extends Component {
  state = {
    visible: false,
    confirmLoading: false,
  };
  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50,
    });
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };
  handleOk = () => {
    this.setState({
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };
  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };
  render() {
    return (
      <div>
        <Row className="section4" justify="center">
          <Col
            data-aos="slide-right"
            data-aos-delay="550"
            xs={24}
            align="center"
            style={{ marginBottom: "2rem" }}
          >
            <Title level={4} style={{ fontSize: "1.9vw" }}>
              Daftar Poin Produk
            </Title>
            <img src={`${window.location.origin}/images/ornament.png`} />
          </Col>
          <Row data-aos="slide-right">
            <Col md={6} align="center">
              <img src={`${window.location.origin}/images/ornament.png`} />
            </Col>
          </Row>
        </Row>
      </div>
    );
  }
}
export default Section4;
