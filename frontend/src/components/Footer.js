import React, { Component } from "react";
import { Row, Col, Typography, Divider } from "antd";
import { Link } from "react-router-dom";
import { inject, observer } from "mobx-react";
import AOS from "aos";
import {
  YoutubeFilled,
  FacebookFilled,
  TwitterSquareFilled,
} from "@ant-design/icons";

const ornament = require("../assets/ornament.png");

@inject("ArticlesStore")
@observer
class Footer extends Component {
  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50,
    });
  };
  render() {
    return (
      <Row className="section9" justify="center">
        <Col md={14}>
          <Row>
            <Typography>Bergabung bersama kami di</Typography>
            <FacebookFilled
              style={{
                color: "#3b5998",
                fontSize: "1.4vw",
              }}
            />
            <Typography>DancowParentingCenter</Typography>
            <TwitterSquareFilled
              style={{
                color: "#3b5998",
                fontSize: "1.4vw",
              }}
            />
            <YoutubeFilled
              style={{
                color: "#3b5998",
                fontSize: "1.4vw",
              }}
            />
          </Row>
          <Row></Row>
        </Col>
        <Col md={5}>
          <img src={ornament} />
        </Col>
        <Col md={5} style={{ marginTop: "3rem" }}>
          <Typography
            style={{
              color: "#23ad46",
              fontSize: "1.1vw",
              fontWeight: "bold",
              paddingBottom: "1rem",
            }}
          >
            Artikel Terbaru
          </Typography>
          <Link to={`/news/1`}>
            <Typography
              style={{
                color: "white",
                paddingBottom: "0.5rem",
              }}
            >
              asdd
            </Typography>
          </Link>
          ))}
        </Col>
      </Row>
    );
  }
}
export default Footer;
