import React, { Component } from "react";
import { observable, action, runInAction } from "mobx";

class ArticlesStore extends Component {
  @observable dataArticles = [];
  @observable loading = true;

  @action
  article() {
    fetch("http://localhost:8000/jsonapi/node/article", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((res) => {
        if (res.data) {
          runInAction(() => {
            this.dataArticles = res.data;
            this.loading = false;
          });
        } else {
          console.log("error");
        }
      })
      .catch((err) => console.log(err));
  }
}

export default new ArticlesStore();
