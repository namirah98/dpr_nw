import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "aos/dist/aos.css";
import "antd/dist/antd.css";
import "./App.scss";
import App from "./App";
import { Provider } from "mobx-react";
import ArticlesStore from "./store/ArticlesStore";
import * as serviceWorker from "./serviceWorker";

const stores = {
  ArticlesStore,
};

ReactDOM.render(
  <Provider {...stores}>
    <App />
  </Provider>,
  document.getElementById("root")
);

serviceWorker.unregister();
