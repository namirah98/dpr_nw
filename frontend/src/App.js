import React, { Component } from "react";
import { Switch, BrowserRouter, Route } from "react-router-dom";
import Home from "./pages/Home";
import Project from "./pages/Project";
import ProjectDetail from "./pages/ProjectDetail";
import About from "./pages/About";
import News from "./pages/News";
import ScrollToTop from "./ScrollToTop";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <ScrollToTop />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/project" component={Project} />
          <Route exact path="/projectdetail" component={ProjectDetail} />
          <Route exact path="/about" component={About} />
          <Route exact path="/news/:id" component={News} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
