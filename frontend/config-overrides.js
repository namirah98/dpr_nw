const {
  override,
  disableEsLint,
  addDecoratorsLegacy,
  fixBabelImports,
  addLessLoader,
} = require("customize-cra");
module.exports = override(
  disableEsLint(),
  addDecoratorsLegacy(),
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true,
  }),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { "@primary-color": "#FF0000" },
  })
);
